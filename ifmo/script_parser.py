#!/usr/bin/env python3

from ifmo.rules import Rule
from functools import partial

class ScriptParser:
  def __init__(self, script):
    if type(script) == str:
      self.script = script.splitlines()
    else: 
      self.script = script

  def extract_rules(self):
    rules = []
    mode = 'search'
    condition_str = ""
    action_lines = []
    for line in self.script:
      if line.startswith('#'):
        continue
      if mode == 'search':
        if line.startswith('RULE '):
          mode = 'rule'
          condition_str = line[5:-1]
          action_lines = []
      elif mode == 'rule':
        if line.rstrip() == 'END RULE':
          rules.append(Rule(condition_str, ''.join(action_lines)))
          mode = 'search'
        else:
          action_lines.append(line)
    return rules

class ScriptRuleMethod:
  def __init__(self, script, script_method, is_begin_or_end = False):
    self.script = script
    self.script_method = script_method
    self.is_begin_or_end = is_begin_or_end
  
  def __call__(self, context, values, output, logger):
    self.script.context = context
    self.script.output = output
    self.script.logger = logger
    if self.is_begin_or_end:
      self.script_method()
    else:
      self.script_method(values)

def parse_py_script(script_classes):
  rules = []
  for s in script_classes:
    script = s()
    if s.begin:
      rules.append(Rule('BEGIN', ScriptRuleMethod(script, script.begin, is_begin_or_end=True)))
    if s.end:
      rules.append(Rule('END', ScriptRuleMethod(script, script.end, is_begin_or_end=True)))
    for func in s.__dict__.values():
      if rule_str := getattr(func, 'rule', None):
        class_func = partial(func, script)
        rules.append(Rule(rule_str, ScriptRuleMethod(script, class_func)))
  return rules

def rule(rule_str):
  def decorate(f):
    setattr(f, "rule", rule_str)
    return f
  return decorate
  

class Script:
  def __init__(self):
    self.output = None
    self.logger = None
    self.context = None

  def begin(self):
    pass
  
  def end(self):
    pass

