#!/usr/bin/env python3

import csv
import sqlite3
import sys
import json

from ifmo.common import open_file

class IfmoOutput:
  def __init__(self, output: str):
    self.output = output
    self.decendants = {}
    self.init_output()

  def __enter__(self):
    return self
  
  def __exit__(self, exc_type, exc_value, traceback):
    self.close_output()
    for d in self.decendants.values():
      if d != self:
        d.__exit__(exc_type, exc_value, traceback)

  def __getitem__(self, key):
    if key in self.decendants:
      return self.decendants[key]
    else:
      writer = self.get_writer_for(key) 
      self.decendants[key] = writer
      return writer

  def init_output(self):
    pass

  def close_output(self):
    pass

  def get_writer_for(self, key):
    return self

  def write(self, arg):
    arg_type = type(arg)
    if arg_type == str:
      self.write_string(arg)
    elif arg_type == list:
      self.write_list(arg)
    elif arg_type == dict:
      self.write_dict(arg)
    else:
      self.write_object(arg)

  def write_string(self, string: str):
    pass

  def write_list(self, in_list: list):
    pass

  def write_dict(self, in_dict: dict):
    pass

  def write_object(self, object):
    self.write_string(str(object))

  def config(self, *args, **kwargs):
    pass

class RawOutput(IfmoOutput):
  def init_output(self):
    self.separator = ' '
    self.wfile = open_file(self.output, 'w')

  def close_output(self):
    if self.wfile:
      self.wfile.close()

  def write_string(self, string: str):
    self.wfile.write(f"{string}\n")

  def write_list(self, in_list: list):
    self.wfile.write(f"{self.separator.join(map(str, in_list))}\n")

  def write_dict(self, in_dict: dict):
    self.write_list(in_dict.items())

  def get_writer_for(self, key):
    return TaggedRawOutput((self, key))

  def config(self, *args, **kwargs):
    if 'separator' in kwargs:
      self.separator = kwargs['separator']

class TaggedRawOutput(IfmoOutput):
  def init_output(self):
    parent = self.output[0]
    self.separator = parent.separator
    self.wfile = parent.wfile
    self.tag = self.output[1]

  def write_string(self, string: str):
    self.wfile.write(f"{self.tag}:{string}\n")

  def write_list(self, in_list: list):
    self.wfile.write(f"{self.tag}:{self.separator.join(map(str, in_list))}\n")

  def write_dict(self, in_dict: dict):
    self.write_list(in_dict.items())

  def get_writer_for(self, key):
    return RawOutput((self, f'{self.tag}:{key}'))

  def config(self, *args, **kwargs):
    if 'separator' in kwargs:
      self.separator = kwargs['separator']

class CsvOutput(IfmoOutput):
  def init_output(self):
    self.wfile = open_file(self.output, 'w')
    self.writer = csv.writer(self.wfile)
    self.dict_col_mapping = {}
    self.dict_col_counter = 0

  def close_output(self):
    if self.wfile:
      self.wfile.close()

  def write_string(self, string: str):
    self.writer.writerow([string])

  def write_list(self, in_list: list):
    self.writer.writerow(in_list)

  def write_dict(self, in_dict: dict):
    for k in in_dict:
      if k not in self.dict_col_mapping:
        self.dict_col_mapping[k] = self.dict_col_counter
        self.dict_col_counter += 1
    
    out_list = [''] * self.dict_col_counter

    for k in in_dict:
      out_list[self.dict_col_mapping[k]] = in_dict[k]

    self.write_list(out_list)
  
  def get_writer_for(self, key):
    return CsvOutput(key)

class BasicJsonOutput(IfmoOutput):
  def init_output(self):
    self.wfile = open_file(self.output, 'w')

  def close_output(self):
    if self.wfile:
      self.wfile.close()

  def write(self, arg):
    self.wfile.write(f"{json.dumps(arg)}\n")

  def get_writer_for(self, key):
    return BasicJsonOutput(key)

class SqliteOutput(IfmoOutput):
  def init_output(self):
    if self.output is None:
      raise Exception("no database selected")
    self.conn = sqlite3.connect(self.output)

  def close_output(self):
    if self.conn:
      self.conn.commit()
      self.conn.close()

  def write(self, arg):
    raise Exception("no table selected")

  def get_writer_for(self, key):
    return SqliteTableOutput((self.conn, key))

class SqliteTableOutput(IfmoOutput):
  def init_output(self):
    self.conn = self.output[0]
    self.table = self.output[1]
    if '"' in self.table:
      raise Exception("invalid character in table name '\"'")
    self.cur = self.conn.cursor()
    self.schema = None

  def write_string(self, string: str):
    if not self.schema:
      self.cur.execute(f'''CREATE TABLE IF NOT EXISTS "{self.table}" ("text" TEXT)''')
      self.schema = {'type': 'str'}

    if self.schema['type'] != 'str':
      raise Exception(f"invalid schema: {self.schema}")
    self.cur.execute(f'''INSERT INTO "{self.table}" ("text") VALUES (?)''', (string,))

  def write_list(self, in_list: list):
    if not self.schema:
      cols = [f"col{i+1}" for i in range(len(in_list))]
      self.cur.execute(f'''CREATE TABLE IF NOT EXISTS "{self.table}" ("{'", "'.join(cols)}")''')
      self.schema = {'type': 'list', 'length': len(in_list)}

    if self.schema['type'] != 'list':
      raise Exception(f"invalid schema: {self.schema}")

    if len(in_list) != self.schema['length']:
      raise Exception(f"invalid list length: {self.schema}")

    self.cur.execute(f'''INSERT INTO "{self.table}" VALUES ({", ".join(["?"] * self.schema['length'])})''', in_list)

  def write_dict(self, in_dict: dict):
    if not self.schema:
      self.cur.execute(f'''CREATE TABLE IF NOT EXISTS "{self.table}" ("{'", "'.join(in_dict.keys())}")''')
      self.schema = {'type': 'dict', 'keys': set(in_dict.keys())}
    
    if self.schema['type'] != 'dict':
      raise Exception(f"invalid schema: {self.schema}")

    if self.schema['keys'] != set(in_dict.keys()):
      raise Exception(f"invalid keys: {self.schema}")

    cols = list(in_dict.keys())
    values = [in_dict[k] for k in cols]

    self.cur.execute(f'''INSERT INTO "{self.table}" ("{'", "'.join(cols)}") VALUES ({", ".join(["?"] * len(values))})''', values)

output_mapping = {
  "raw": RawOutput,
  "csv": CsvOutput,
  "basic_json": BasicJsonOutput,
  "sqlite": SqliteOutput
}
  
