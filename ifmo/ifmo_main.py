#!/usr/bin/env python3


from argparse import ArgumentParser, FileType
import re
import csv
import sys
import logging


from functools import partial
from ifmo.output import output_mapping
from ifmo.input import input_mapping
from ifmo.filtermap import Rule, filtermap_mappings, Context
from ifmo.script_parser import ScriptParser, Script, rule, parse_py_script

LOG = logging.getLogger(__name__)

def getArgs():
  parser = ArgumentParser()
  parser.add_argument("--output_type", "-O", default="raw")
  parser.add_argument("--output", "-o")
  parser.add_argument("--input_type", "-I", default="text")
  parser.add_argument("--filtermap_type", "-F", default="awk")
  parser.add_argument("--script", "-s")
  parser.add_argument("--script_file", "-S", type=FileType('r'))
  parser.add_argument("--verbose", "-v", action="store_true")
  parser.add_argument("input_file", nargs="*")
  return parser.parse_args()

def run_main_loop(args, init_rules = []):
  output_type = output_mapping[args.output_type]
  input_type = input_mapping[args.input_type]
  filtermap_type = filtermap_mappings[args.filtermap_type]

  rules = []

  if args.script:
    rules += ScriptParser(args.script).extract_rules()
  
  if args.script_file:
    with args.script_file as script_file:
      rules += ScriptParser(script_file).extract_rules()

  rules += init_rules

  if len(rules) == 0:
    raise Exception("no rules")

  begin_rules = [rule for rule in rules if rule.condition_string == 'BEGIN']
  end_rules = [rule for rule in rules if rule.condition_string == 'END']
  real_rules = [rule for rule in rules if rule.condition_string not in ['BEGIN', 'END']]

  context = Context()
  context.num_files = len(args.input_file)

  LOG.debug("initializing %s with %s for output", output_type, args.output)
  with output_type(args.output) as output:
    filtermap = filtermap_type(real_rules, output)
    LOG.debug("executing begin rules")
    for rule in begin_rules:
      rule.eval(context, None, output)

    for num_file, input_file in enumerate(args.input_file):
      context.file_index = num_file
      context.current_file = input_file
      LOG.debug("using %s to open file %s (file# %s)", input_type, input_file, num_file)
      with input_type(input_file) as input:
        for num_entry, entry in enumerate(input.entries()):
          context.entry_index = num_entry
          context._skip_file = False
          filtermap.filter_map_entry(entry, context)
          if context._skip_file:
            break
    
    LOG.debug("executing end rules")
    for rule in end_rules:
      rule.eval(context, None, output)

def run_script(*scripts):
  rules = parse_py_script(scripts)
  main(init_rules=rules)

def main(init_rules = []):
  args = getArgs()
  log_format = '%(asctime)s - %(levelname)s - %(message)s'
  log_level = logging.DEBUG if args.verbose else logging.ERROR
  logging.basicConfig(format=log_format, level=log_level)

  run_main_loop(args, init_rules)

if __name__ == '__main__':
  main()


