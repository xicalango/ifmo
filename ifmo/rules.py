#!/usr/bin/env python3

from logging import getLogger

class CompiledAction:
  def __init__(self, action: str, condition_string: str):
    self.compiled_action = compile(action, f'action for {condition_string}', 'exec')
  
  def __call__(self, context, values, output, logger):
    exec(self.compiled_action, {'context': context, 'values': values, 'output': output, 'logger': logger})

def get_action_callable(condition_string: str, action):
  if type(action) == str:
    return CompiledAction(action, condition_string)
  else:
    return action

class Rule:
  def __init__(self, condition_string: str, action):
    self.condition_string = condition_string
    self.action = get_action_callable(condition_string, action)
    self.logger = getLogger(self.condition_string)
    self.init_condition()

  def init_condition(self):
    pass

  def eval(self, context, values, output):
    self.action(context, values, output, self.logger)

  def filter_map(self, entry):
    pass

