#!/usr/bin/env python3

from typing import List
from ifmo.common import open_file
from ifmo.output import IfmoOutput

class IfmoInput:
  def __init__(self, input):
    self.input = input
    self.init_input()

  def __enter__(self):
    return self

  def __exit__(self, exc_type, exc_value, traceback):
    self.close_input()

  def init_input(self):
    pass

  def close_input(self):
    pass

  def entries(self):
    pass

class TextInput(IfmoInput):
  def init_input(self):
    self.rfile = open_file(self.input, 'r')

  def close_input(self):
    if self.rfile:
      self.rfile.close()

  def entries(self):
    for line in self.rfile:
      yield line.rstrip()

input_mapping = {
  "text": TextInput
}
