#!/usr/bin/env python3

from collections import deque
from typing import List
import logging
from ifmo.output import IfmoOutput
from ifmo.rules import Rule

import re

LOG = logging.getLogger(__name__)

class Context:
  def __init__(self):
    self.entry = None
    self.transformed_entry = None
    self.entry_index = 0

    self._skip_entry = False
    self._skip_file = False

    self.num_files = 0
    self.current_file = None
    self.file_index = 0

    self.vars = {}

    self.history = None

  def skip_entry(self):
    self._skip_entry = True

  def skip_file(self):
    self._skip_file = True

  def set_history_length(self, length = 0):
    if length == 0:
      self.history = None
    else:
      self.history = deque([], length)

  def __getitem__(self, key):
    return self.vars.get(key)

  def __setitem__(self, key, value):
    self.vars[key] = value

class IfmoFilterMap:
  def __init__(self, rules: List[Rule], output: IfmoOutput):
    self.rules = [self.transform_rule(rule) for rule in rules if rule.condition_string not in ["BEGIN", "END"]]
    self.output = output

  def transform_rule(self, rule: Rule) -> Rule:
    return rule

  def transform_entry(self, entry):
    return entry

  def filter_map_entry(self, entry, context: Context):
    transformed_entry = self.transform_entry(entry)
    for rule in self.rules:
      values = rule.filter_map(transformed_entry)
      if values is not None:
        context.entry = entry
        context.transformed_entry = transformed_entry
        context._skip_entry = False
        rule.eval(context, values, self.output)
        if context._skip_entry:
          break

    if context.history is not None:
      context.history.append(transformed_entry)

class AwkRule(Rule):
  def init_condition(self):
    self.pattern = re.compile(self.condition_string)

  def filter_map(self, entry):
    matched = self.pattern.match(entry)
    if matched is None:
      return None
    else:
      if matched.groupdict():
        return matched.groupdict()
      else:
        return matched.groups()

class AwkFilterMap(IfmoFilterMap):

  def transform_rule(self, rule: Rule) -> Rule:
    return AwkRule(rule.condition_string, rule.action)


filtermap_mappings = {
  'awk': AwkFilterMap
}
