#!/usr/bin/env python3

import sys

def open_file(filename, mode):
  if filename == '-' or filename is None:
    if mode == 'w':
      return sys.stdout
    elif mode == 'r':
      return sys.stdin
    else:
      raise Exception()
  else:
    return open(filename, mode)

