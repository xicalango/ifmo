#!/usr/bin/env python3

from setuptools import find_packages, setup

setup(
  name='ifmo',
  packages=find_packages(),
  version='0.1.3',
  description='Like awk on steroids!',
  author='Alexander Weld',
  license='MIT'
)

